#!/usr/bin/env bash

set -eu


bash --version

docker --version

envsubst --version

jq --version

helm version --client

k3s --version
