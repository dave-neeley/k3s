# Synopsis

Racher k3s (certified Kubernetes distro) plus some tools around it.  
Published to Docker Hub at: https://hub.docker.com/r/softmill/k3s


# What's inside

The best way to see current list of tools is to check [test.sh](test.sh)


# Usage locally

```bash
$ docker run -it -v /var/run/docker.sock:/var/run/docker.sock --privileged --rm softmill/k3s
/ # start_kube.sh

...

/ # kubectl get all
NAME                 TYPE        CLUSTER-IP   EXTERNAL-IP   PORT(S)   AGE
service/kubernetes   ClusterIP   10.43.0.1    <none>        443/TCP   3m36s

/ # kubectl get node
NAME           STATUS   ROLES    AGE     VERSION
c574a1764aa3   Ready    <none>   3m46s   v1.14.1-k3s.4
```


# Usage in GitLab CI

This image can be used in CI\CD pipelines to test Kubernetes deployments
without actual Kubernetes cluster running.  
See example of usage in:
https://gitlab.com/softmill/containers/go-webapp-on-kubernetes/blob/master/.gitlab-ci.yml

Essential part of `.gitlab-ci.yml` is:
```yaml
install:
  image: softmill/k3s
  services:
    - name: docker:dind
  script:
    - start_kube.sh
    - helm install ...
```


# References

* https://k3s.io/
* https://github.com/rancher/k3s
* https://github.com/rancher/k3s/blob/master/package/Dockerfile
* https://github.com/rancher/k3s#auto-deploying-manifests
* https://github.com/alpine-docker/helm/blob/master/Dockerfile
* https://helm.sh/docs/using_helm/#tiller-and-role-based-access-control
