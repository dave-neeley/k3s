#!/usr/bin/env bash
#
# This script tests simple deployment to Kubernetes, following this guide:
# https://kubernetes.io/docs/tasks/run-application/run-stateless-application-deployment/
#

set -eu


echo "--- Deployment to the cluster ..."
kubectl apply --wait -f deployment.yaml

DEPLOYMENT_ID=deployment.apps/nginx-deployment  # must match name from the .yaml file

echo "--- Waiting for rollout to succeed"
kubectl rollout status $DEPLOYMENT_ID --timeout=60s
RESULT=$?

echo "--- Describing deployment"
kubectl describe $DEPLOYMENT_ID

if [[ $RESULT != 0 ]]
then
    echo "Exiting with failure ..."
    exit 1
fi
