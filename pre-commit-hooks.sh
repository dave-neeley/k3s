#!/usr/bin/env bash

set -e


pip -q install pre-commit

pre-commit install

pre-commit autoupdate

pre-commit run --all-files
