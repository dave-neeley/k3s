FROM alpine

# Install Alpine packages

RUN apk add --no-cache --update \
        bash \
        curl \
        docker \
        gettext \
        jq \
    && rm -rf /var/cache/apk/*


# Download tools

ARG TOOLS_TARGET_DIR=/usr/bin

ARG K3S_VERSION=v1.0.1
ADD https://github.com/rancher/k3s/releases/download/${K3S_VERSION}/k3s $TOOLS_TARGET_DIR
RUN cd $TOOLS_TARGET_DIR && \
    chmod +x k3s && \
    ln -s k3s kubectl
ADD start_kube.sh $TOOLS_TARGET_DIR


ARG HELM_VERSION=v3.2.0
ARG HELM_URL=https://get.helm.sh/helm-${HELM_VERSION}-linux-amd64.tar.gz
RUN curl -L $HELM_URL | tar xvz && \
    mv linux-amd64/helm $TOOLS_TARGET_DIR && \
    chmod +x ${TOOLS_TARGET_DIR}/helm && \
    rm -rf linux-amd64


VOLUME /var/lib/rancher/k3s
VOLUME /var/lib/cni
VOLUME /var/log


# Run tests

COPY test_smoke.sh .
RUN ./test_smoke.sh
