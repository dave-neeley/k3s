#!/usr/bin/env bash

set -eux

# Start Kubernetes server
k3s server &
sleep 30
kubectl wait pod --for=condition=Ready -n kube-system -l k8s-app=kube-dns

cp /etc/rancher/k3s/k3s.yaml ~/.kube/config
helm version
